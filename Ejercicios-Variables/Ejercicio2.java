/**
 * Ejercicio 2 y 3 de variables de Aprende Java con Ejercicios
*/

public class Ejercicio2 {
	public static void main(String[] args) {
		String nombre = "Mauricio Suarez Barrera";
		int telefono = 54230768;
		String direccion = "Mz6 Lt2 Calle Pajuil Col. Tepeaca";
		System.out.println(nombre);
		System.out.println(telefono);
		System.out.println(direccion);
	}
}